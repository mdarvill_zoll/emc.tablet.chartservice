﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace EMC.CouchDb.ChartManifestTimeout.Observable
{
    public interface ICheckForWaitingDocuments
    {
        bool Touch { get; set; }

        void Start(TimeoutHandler timeoutHandler);

        void Stop();

        public long IntervalTimeOut { get; set; }
    }

    public delegate void TimeoutHandler();
}
