﻿using EMC.CouchDb.Document.V1;
using EMC.Tablet.Documents.V1;
using log4net;
using System;
using System.Collections.Generic;

namespace EMC.CouchDb.ChartManifestTimeout.Observable
{
    public class Observer : IObserver<ChartManifestDocument>
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Observer));
        private IDisposable _unsubscriber;

        public string Name { get; set; } = "<Undefined>";

        public Observer(string name)
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name), "The observer must be assigned a name.");

            this.Name = name;
        }

        public virtual void Subscribe(Observable provider)
        {
            _unsubscriber = provider.Subscribe(this);
        }

        public virtual void Unsubscribe()
        {
            _unsubscriber.Dispose();
        }

        public virtual void OnCompleted()
        {
            _log.Debug($"OnCompleted {Name}: I am done listening for new Observable changes.");
        }

        // No implementation needed: Method is not called by the BaggageHandler class.
        public virtual void OnError(Exception e)
        {
            _log.Debug($"OnError {Name}: An unexpected error has been caught.", e);
        }

        // Update information.
        public virtual void OnNext(ChartManifestDocument document)
        {
            _log.Debug($"Starting OnNext {Name}:  Starting OnNext.");
        }
    }
}
