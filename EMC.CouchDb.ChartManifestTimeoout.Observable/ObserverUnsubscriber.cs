﻿using log4net;
using System;
using System.Collections.Generic;

namespace EMC.CouchDb.ChartManifestTimeout.Observable
{
    public sealed class ObserverUnsubscriber<ChartManifestDocument> : IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ObserverUnsubscriber<ChartManifestDocument>));
        private List<IObserver<ChartManifestDocument>> _observers;
        private IObserver<ChartManifestDocument> _observer;

        public ObserverUnsubscriber(List<IObserver<ChartManifestDocument>> observers, IObserver<ChartManifestDocument> observer)
        {
            _log.Debug($"Adding observer {observer}");
            this._observers = observers;
            this._observer = observer;
        }

        public void Dispose()
        {
            _log.Debug($"Disposing of observer {_observer}");
            if (_observers.Contains(_observer))
            {
                _observers.Remove(_observer);
            }
        }
    }
}
