﻿using EMC.CouchDb.Document.V1;
using EMC.Tablet.ChartService.Common;
using log4net;
using MyCouch;
using MyCouch.Requests;
using System;
using System.Collections.Generic;
using System.Timers;

namespace EMC.CouchDb.ChartManifestTimeout.Observable
{
    public class Observable : IObservable<ChartManifestDocument>
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(IObservable<ChartManifestDocument>));
        private readonly List<IObserver<ChartManifestDocument>> observers = new List<IObserver<ChartManifestDocument>>();
        private readonly Configuration _config;
        private readonly ChartManifestTimedOut _changesTimerdOutTimer;
        private MasterConnection _masterConnection;

        public Observable(Configuration config)
        {
            _config = config ?? throw new ArgumentNullException(nameof(config));

            _masterConnection = config.GetSection<MasterConnection>(Configuration.SectionCompletionDatabase);
            _changesTimerdOutTimer  =new ChartManifestTimedOut();
        }

        public void Start()
        {
            _changesTimerdOutTimer.GetTimer(CheckForTimedOutChanges);
        }

        public IDisposable Subscribe(IObserver<ChartManifestDocument> observer)
        {
            if (observer == null)
                throw new ArgumentNullException(nameof(observer));

            if (observers.Contains(observer))
                throw new Exception($"{observer} already exists");

             observers.Add(observer);
             return new ObserverUnsubscriber<ChartManifestDocument>(observers, observer);
        }

        public async void CheckForTimedOutChanges(object sender, ElapsedEventArgs args)
        {
            try
            {

                using(IMyCouchClient client = _masterConnection.Create()) {

                    QueryViewRequest request = new QueryViewRequest();

                    client.Views.QueryAsync<ChartManifestDocument>(request)

                }
                _log.Debug("OnTimedOut: Restarting the listener.");

            }
            catch (Exception ex)
            {
                _log.Error("OnTimedOut: While handling OnTimedOut an unexpected exception was handled.", ex);
                _log.Error("OnTimedOut: The application will try again later.");
            }
        }

        public void NotifyChange(ChartManifestDocument document)
        {
            _log.Debug($"Starting NotifyChange change.");

            if (document == null)
                return;

            try
            {
                if (document.Ignore)
                {
                    return;
                }

                foreach (var observer in observers)
                    observer.OnNext(document);
            } 
            catch(Exception ex)
            {
                _log.Warn("Stopping NotifyChange: Unknown Exception.", ex);
            }
        }
    }
}