﻿using log4net;
using MyCouch;
using System;
using System.Timers;
using token = System.Threading;

namespace EMC.CouchDb.ChartManifestTimeout.Observable
{
    public sealed class ChartManifestTimedOut: ICheckForWaitingDocuments, IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ChartManifestTimedOut));
        private TimeoutHandler _onIntervalComplete;
        private Timer _timer;
        private token.CancellationTokenSource _cancellationTokenSource;

        public ChartManifestTimedOut()
        {
            _cancellationTokenSource = new token.CancellationTokenSource();
        }

        public bool Touch { get; set; }

        public long IntervalTimeOut { get; set; } = 30000;

        public Timer GetTimer(ElapsedEventHandler intervalComplete)
        {
            _timer = new Timer
            {
                AutoReset = true,
                Interval = IntervalTimeOut
            };

            _timer.Elapsed += new ElapsedEventHandler(intervalComplete);

            return _timer;
        }

        public void Stop()
        {
            if (_timer == null)
                return;

            _timer.Stop();
        }

        public void Dispose()
        {
            if (_timer != null)
            {
                _timer.Stop();
                _timer.Dispose();
            }
               
        }
    }
}
