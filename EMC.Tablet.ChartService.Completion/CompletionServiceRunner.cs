﻿using EMC.CouchDb.Sequence.Observable;
using EMC.Tablet.ChartService.Common;
using EMC.Tablet.ChartService.Completion.Handlers;
using log4net;
using System;
using System.Collections.Generic;
using System.Threading;

namespace EMC.Tablet.ChartService.Completion
{
    public class CompletionServiceRunner: IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(CompletionServiceRunner));
        IList<IDisposable> _suscribedObservers = new List<IDisposable>();
        CancellationToken _token = new CancellationToken();

        public CompletionServiceRunner()
        {

        }

        public void Initialize(CancellationToken token)
        {
            _token = token;

            Configuration config = new Configuration("EMC.Tablet.ChartCompleteService.json");

            Observable sequenceObservable = new Observable(config);
            
            Observer sequenceObserver = new Observer("Chart Manifest Observer");
            
           
            // todo: we may need another observer for ServiceReportDocuments
            // _suscribedObservers.Add(sequenceObservable.Subscribe(sequenceObservable));

            var masterConnection = config.GetSection<MasterConnection>(Configuration.SectionCompletionDatabase).Create();
            //ILockChanges changesLocker = new LockChanges(masterConnection);

            var readyHandler = new ReadyHandler();

            var verifyHandler = new VerifyHandler(config)
            {
                Success = new StateActivity("ready", "upload"),
                Failure = new StateActivity("ready", "notify"),
                Error = new StateActivity("error", ""),
            };
            readyHandler.Register(verifyHandler);

            var uploadHandler = new UploadHandler(config)
            {
                Success = new StateActivity("ready", "nemsis"),
                Failure = new StateActivity("ready", "notify"),
                Error = new StateActivity("error", ""),
            };
            readyHandler.Register(uploadHandler);

            var nemsisHandler = new NemsisHandler(config)
            {
                Success = new StateActivity("complete", "delete"),
                Failure = new StateActivity("ready", "notify"),
                Error = new StateActivity("error", ""),
            };
            readyHandler.Register(nemsisHandler);

            var notifyHandler = new NotifyHandler(config)
            {
                Success = new StateActivity("not-ready", ""),
                Failure = new StateActivity("not-ready", ""),
                Error = new StateActivity("error", ""),
            };
            readyHandler.Register(notifyHandler);

            sequenceObserver.Register(readyHandler);

            var completehandler = new CompleteHandler();

            var deleteHandler = new DeleteHandler(config)
            {
                Success = new StateActivity("complete", "notify"),
                Failure = new StateActivity("error", ""),
                Error = new StateActivity("error", ""),
            };
            completehandler.Register(deleteHandler);

            var completeNotify = new NotifyHandler(config)
            {
                Success = new StateActivity("complete", ""),
                Failure = new StateActivity("not-ready", ""),
                Error = new StateActivity("error", ""),
            };
            completehandler.Register(completeNotify);

            sequenceObserver.Register(completehandler);
            _suscribedObservers.Add(sequenceObservable.Subscribe(sequenceObserver));

            sequenceObservable.Start();
        }

        public void Dispose()
        {
            _log.Debug($"{nameof(CompletionServiceRunner)} is disposing of registered observers.");
            foreach (var subscribedObserver in _suscribedObservers)
            {
                subscribedObserver.Dispose();
            }
        }
    }
}
