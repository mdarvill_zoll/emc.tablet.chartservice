﻿using EMC.CouchDb.Document.V1;
using EMC.Tablet.Documents.V1;
using MyCouch;
using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace EMC.Tablet.ChartService.Completion
{
    public static class HandlerHelper
    {
        public async static Task<string> GetDocument(this IMyCouchClient client, string docId)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            if(string.IsNullOrWhiteSpace(docId))
                throw new ArgumentNullException(nameof(docId));

            //todo: we may need to add a retry in here if there is a failure.
            var response = await client.Documents.GetAsync(docId);

            if(!response.IsSuccess)
                throw new Exception($"GetDocument has encountered a problem while retrieving a document, {response.Reason}");

            if (response.IsEmpty)
                return string.Empty;

            return response.Content;
        }

        public async static Task Lock(this IMyCouchClient client, string seq, ChartManifestDocument manifest)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            if (string.IsNullOrWhiteSpace(seq))
                throw new ArgumentNullException(nameof(seq));

            var doc = new LockDocument(DocumentTypes.LockingDoc, manifest.State, seq, manifest.ErrorRetries);

            //todo: we may need to add a retry in here if there is a failure.
            var response = await client.Documents.PostAsync(doc.ToStringObject());

            if (!response.IsSuccess && !response.Reason.Contains("conflict"))
                throw new Exception($"GetDocument has encountered a problem while retrieving a document, {response.Reason}");
        }

        public async static Task<bool> Save(this IMyCouchClient client, ChartManifestDocument manfest)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            if (manfest == null)
                throw new ArgumentNullException(nameof(manfest));

            //todo: we may need to add a retry in here if there is a failure.
            var response = await client.Documents.PutAsync(manfest.Id, manfest.ToStringObject());

            if (!response.IsSuccess && !response.Reason.Contains("conflict"))
                throw new Exception($"GetDocument has encountered a problem while retrieving a document, {response.Reason}");
            else if (!response.IsSuccess && response.Reason.Contains("conflict"))
                return false;
            else
                return true;
        }

        public static ChartManifestDocument Clean(this ChartManifestDocument manifest)
        {
            if (manifest == null)
                throw new ArgumentNullException(nameof(manifest));

            var newManifest = new ChartManifestDocument
            {
                Id = manifest.Id,
                Revision = manifest.Revision,
                Manifest = manifest.Manifest,
                State = manifest.State,
                ServiceId = manifest.ServiceId,
                UserId = manifest.UserId
            };

            if(manifest.TraceOn)
            {
                newManifest.TraceOn = true;
                newManifest.Trace = manifest.Trace;
            }

            return newManifest;
        }
    }
}
