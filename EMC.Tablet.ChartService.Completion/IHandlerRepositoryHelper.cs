﻿using EMC.CouchDb.Document.V1;
using Newtonsoft.Json.Linq;

namespace EMC.Tablet.ChartService.Completion
{
    public interface IHandlerRepositoryHelper
    {
        JObject GetDocument(string docId);

        JObject SaveManifest(ChartManifestDocument manfest);

        JObject LockDocument(string seq, ChartManifestDocument manfest);
    }
}