﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMC.Tablet.ChartService.Completion
{
    public class StateActivity
    {
        public StateActivity(string state, string activity)
        {
            if (string.IsNullOrWhiteSpace(state))
                throw new ArgumentNullException(nameof(state));

            State = state;
            Activity = activity;
        }

        public string State { get; set; }

        public string Activity { get; set; }
    }
}
