﻿using EMC.CouchDb.Document.V1;

namespace EMC.Tablet.ChartService.Completion
{
    public interface IHandleChartManifest
    {
        string HandlerName { get; set; }

        StateActivity Success { get; set; }

        StateActivity Failure { get; set; }

        StateActivity Error { get; set; }

        /// <summary>
        /// The handler executes specific code against the ChartMandifest document.
        /// </summary>
        /// <param name="seq">The sequence number of the last request</param>
        /// <param name="manifest">The ChartManifest of the change being handled.</param>
        void Handler(string seq, ChartManifestDocument manifest);
    }
}