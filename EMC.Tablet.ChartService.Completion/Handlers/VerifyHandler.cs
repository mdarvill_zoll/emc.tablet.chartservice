﻿using EMC.CouchDb.Document.V1;
using EMC.Tablet.ChartService.Common;
using log4net;
using MyCouch;
using System;

namespace EMC.Tablet.ChartService.Completion.Handlers
{
    public class VerifyHandler : IHandleChartManifest
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(VerifyHandler));
        private Configuration _config;
        private readonly IMyCouchClient _connectionDb;

        public VerifyHandler(Configuration config)
        {
            _config = config ?? throw new ArgumentNullException(nameof(config));

            _connectionDb = _config.GetSection<MasterConnection>(Configuration.SectionCompletionDatabase).Create();
        }

        public StateActivity Success { get; set; }

        public StateActivity Failure { get; set; }

        public StateActivity Error { get; set; }

        public  string HandlerName { get; set; } = "verify";

        public async void Handler(string seq, ChartManifestDocument manifest)
        {
            _log.Info($"{HandlerName} is responding");

            //IMyCouchClient _client =_config.GetSection<UserConnection>("userDatabase").Create(manifest.UserId);

            try
            {
                await _connectionDb.Lock(seq, manifest);

                var newManifest = manifest.Clean();

                //success
                newManifest.State = Success.State;
                newManifest.ChartActivity = Success.Activity;

                await _connectionDb.Save(newManifest);

            }
            catch (Exception ex)
            {
                _log.Info("verify is locked.");
            }
        }
    }
}
