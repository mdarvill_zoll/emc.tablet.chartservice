﻿using EMC.CouchDb.Document.V1;
using EMC.Tablet.ChartService.Common;
using log4net;
using MyCouch;
using System;

namespace EMC.Tablet.ChartService.Completion.Handlers
{
    public class NotifyHandler : IHandleChartManifest
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(NemsisHandler));
        private Configuration _config;
        private readonly IMyCouchClient _connectionDb;

        public NotifyHandler(Configuration config)
        {
            _config = config ?? throw new ArgumentNullException(nameof(config));
            _connectionDb = config.GetSection<MasterConnection>(Configuration.SectionCompletionDatabase).Create();
        }

        public StateActivity Success { get; set; }

        public StateActivity Failure { get; set; }

        public StateActivity Error { get; set; }

        public string HandlerName { get; set; } = "notify";

        public async void Handler(string seq, ChartManifestDocument manifest)
        {
            _log.Info($"{HandlerName} is responding");

           // IMyCouchClient client = _config.GetSection<UserConnection>("userDatabase").Create(manifest.UserId);

            try
            {
                await _connectionDb.Lock(seq, manifest);

                var newManifest = manifest.Clean();

                newManifest.State = Success.State;
                newManifest.ChartActivity = Success.Activity;

                await _connectionDb.Save(newManifest);
            }
            catch (Exception ex)
            {
                _log.Info("notify is locked.");
            }
        }
    }
}
