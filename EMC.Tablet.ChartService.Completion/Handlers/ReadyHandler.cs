﻿using EMC.CouchDb.Document.V1;
using EMC.CouchDb.Sequence.Observable;
using log4net;
using System;
using System.Collections.Generic;

namespace EMC.Tablet.ChartService.Completion.Handlers
{
    public class ReadyHandler : ISequenceHandler, IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ReadyHandler));

        private Dictionary<string, IHandleChartManifest> _activeChartActivityHandlers;

        public ReadyHandler()
        {
            _log.Debug($"Creating the {HandlerName} handler.");

            _activeChartActivityHandlers = new Dictionary<string, IHandleChartManifest>();

            //todo we need to add the other handlers that are part of this here
        }

        public void Register(IHandleChartManifest handler)
        {
            _log.Info($"Registering the {handler.HandlerName} handler to {HandlerName}.");
            _activeChartActivityHandlers.Add(handler.HandlerName, handler);
        }

        public string HandlerName { get; set; } = "ready";

        public void Handle(SequenceDocument message)
        {
            _log.Info($"{HandlerName} handler is started.");
            ChartManifestDocument manifest = message.Doc.ToObject<ChartManifestDocument>();

            _log.Debug($"A sequence document has successfully been serialized to a seq: {message.Seq}, id: {manifest.Id}, state: {manifest.State}, chartActivity: {manifest.ChartActivity }");

            if (_activeChartActivityHandlers.ContainsKey(manifest.ChartActivity))
            {
                _activeChartActivityHandlers[manifest.ChartActivity].Handler(message.Seq, manifest);
            }
        }

        public void Dispose()
        {
            _log.Debug($"Disposing the {HandlerName} handler.");
            _activeChartActivityHandlers = null;
        }
    }
}
