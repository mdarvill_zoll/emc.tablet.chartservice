﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMC.Tablet.CompletioneService
{
    public interface IHandlerRepoHelper
    {
        JObject GetDocument(string docId);
    }
}
