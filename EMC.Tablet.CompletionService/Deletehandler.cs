﻿using EMC.CouchDb.Document.V1;
using log4net;

namespace EMC.Tablet.CompletioneService
{
    public class Deletehandler : IHandleChartManifest
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Deletehandler));

        public string HandlerName { get; set; } = "delete";

        public void Handler(ChartManifestDocument manifest)
        {
            _log.Info("delete detected");
        }
    }
}
