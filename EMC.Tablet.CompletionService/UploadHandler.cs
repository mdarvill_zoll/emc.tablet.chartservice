﻿using EMC.CouchDb.Document.V1;
using log4net;

namespace EMC.Tablet.ChartCompleteService
{
    public class UploadHandler : CompletioneService
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(UploadHandler));

        public string HandlerName { get; set; } = "upload";

        public void Handler(ChartManifestDocument manifest)
        {
            _log.Info("upload detected");
        }
    }
}
