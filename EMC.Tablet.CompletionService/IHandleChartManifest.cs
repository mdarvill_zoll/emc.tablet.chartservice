﻿using EMC.CouchDb.Document.V1;

namespace EMC.Tablet.CompletioneService
{
    public interface IHandleChartManifest
    {
        string HandlerName { get; set; }

        void Handler(ChartManifestDocument manifest);
    }
}