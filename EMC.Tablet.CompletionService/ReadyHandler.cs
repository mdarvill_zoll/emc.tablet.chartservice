﻿using EMC.CouchDb.Document.V1;
using log4net;
using System;
using System.Collections.Generic;

namespace EMC.Tablet.CompletionService
{
    public class ReadyHandler : ISequenceHandler, IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(VerifyHandler));

        private Dictionary<string, IHandleChartManifest> _activeChartActivityHandlers;

        public ReadyHandler()
        {
            _log.Debug($"Creating the {HandlerName} handler.");

            _activeChartActivityHandlers = new Dictionary<string, IHandleChartManifest>();

            //todo we need to add the other handlers that are part of this here
        }

        public void Register(IHandleChartManifest handler)
        {
            _log.Info($"Registering the {handler.HandlerName} handler to {HandlerName}.");
            _activeChartActivityHandlers.Add(handler.HandlerName, handler);
        }

        public string HandlerName { get; set; } = "ready";

        public void Handle(SequenceDocument message)
        {
            _log.Info($"A sequence document is being handled by the {HandlerName} handler.");

            ChartManifestDocument manifest = message.Doc.ToObject<ChartManifestDocument>();

            _log.Info($"A sequence document has successfully been serialized to a ChartManifest {manifest.Id}, {manifest.State}, {manifest.ChartActivity }");

            if (_activeChartActivityHandlers.ContainsKey(manifest.ChartActivity))
            {
                _activeChartActivityHandlers[manifest.ChartActivity].Handler(manifest);
            }
        }

        public void Dispose()
        {
            _log.Debug($"Disposing the {HandlerName} handler.");
            _activeChartActivityHandlers = null;
        }
    }
}
