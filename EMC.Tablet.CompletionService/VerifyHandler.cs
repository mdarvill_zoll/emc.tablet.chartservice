﻿using EMC.CouchDb.Document.V1;
using EMC.Tablet.CompletioneService;
using log4net;

namespace EMC.Tablet.CompletionService
{
    public class VerifyHandler : IHandleChartManifest
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(VerifyHandler));

        public string HandlerName { get; set; } = "verify";

        public void Handler(ChartManifestDocument manifest)
        {
            _log.Info("verify detected");
        }
    }
}
