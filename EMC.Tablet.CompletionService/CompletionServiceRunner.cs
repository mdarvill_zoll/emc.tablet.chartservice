﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMC.Tablet.CompletionService
{
    public class CompletionServiceRunner
    {
        public CompletionServiceRunner()
        {

        }

        public void Initialize()
        {
            SequenceObservable sequenceObservable = new SequenceObservable(completionDatabase);
            SequenceObserver sequenceObserver = new SequenceObserver("Chart Manifest Observer");
            var subscribe = sequenceObservable.Subscribe(sequenceObserver);

            ILockChanges changesLocker = new LockChanges(completionDatabase.CreateCouchClient());



            var readyHandler = new ReadyHandler();
            readyHandler.Register(new VerifyHandler());
            readyHandler.Register(new UploadHandler());
            readyHandler.Register(new NemsisHandler());
            readyHandler.Register(new NotifyHandler());

            sequenceObserver.Register(readyHandler);

            var completehandler = new CompleteHandler();

            completehandler.Register(new Deletehandler());
            completehandler.Register(new NotifyHandler());

            sequenceObserver.Register(completehandler);
            sequenceObservable.Start();
        }
    }
}
