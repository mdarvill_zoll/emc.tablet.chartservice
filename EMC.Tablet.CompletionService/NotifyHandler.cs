﻿using EMC.CouchDb.Document.V1;
using log4net;

namespace EMC.Tablet.ChartCompleteService
{
    public class NotifyHandler : IHandleChartManifest
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(NotifyHandler));

        public string HandlerName { get; set; } = "notify";

        public void Handler(ChartManifestDocument manifest)
        {
            _log.Info("notify detected");
        }
    }
}
