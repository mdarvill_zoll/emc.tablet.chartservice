﻿using EMC.CouchDb.Sequence.Observable;
using EMC.Tablet.ChartService.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MyCouch;
using MyCouch.Requests;
using MyCouch.Responses;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace EMS.CouchDd.Observer.Tests
{
    [TestClass]
    public class ChangeMonitorTests
    {
        Configuration _config;
        MasterConnection masterConfig;

        public ChangeMonitorTests()
        {
            _config = new Configuration();
            _config.Load(JObject.FromObject(new
            {
                completionDatabase = JObject.FromObject(new
                {

                    BaseUrl = "http://a954c9f26509.ngrok.io ",
                    Database = "completiondb",
                    Password = "!Password1",
                    Username = "administrator",
                    PollingWait = 30
                }),
                userDatabase = JObject.FromObject(new
                {
                    BaseUrl = "http://a954c9f26509.ngrok.io ",
                    DatabasePrefix = "um_",
                    Password = "!Password1",
                    Username = "administrator",
                    PollingWait = 30
                })
            }));

            masterConfig = _config.GetSection<MasterConnection>(Configuration.SectionCompletionDatabase);
        }
        [TestMethod]
        public void CreatingTheMonitorWithMinimumConfigurationThrowsNoExceptions()
        {

            ChangesMonitor monitor = new ChangesMonitor(_config);
        }

        [TestMethod]
        public void ListenerStartsTimerAndChangeFeed()
        {
            MasterConnection config = new MasterConnection
            {
                Username = "a",
                Password = "a",
                Database = "a",
                BaseUrl = "http://s.com"
            };

            Mock<IMyCouchClient> moqCouchClient = new Mock<IMyCouchClient>();
            Mock<IChanges> moqChanges = new Mock<IChanges>();
            Mock<IMonitorChangesTimeout> moqTimer = new Mock<IMonitorChangesTimeout>();
            Mock<ILockChanges> moqLock = new Mock<ILockChanges>();

            moqCouchClient.Setup(m => m.Changes).Returns(moqChanges.Object);

            ChangesMonitor monitor = new ChangesMonitor(moqCouchClient.Object, moqTimer.Object, moqLock.Object, config);

            monitor.Listener();

            moqTimer.Verify(m => m.Start(It.IsAny<TimeoutHandler>()), Times.Once);
            moqChanges.Verify(m => m.GetAsync(It.IsAny<GetChangesRequest>(), It.IsAny<Action<string>>(), It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
