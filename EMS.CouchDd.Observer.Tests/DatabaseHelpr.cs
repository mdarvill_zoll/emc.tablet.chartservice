﻿using MyCouch;
using MyCouch.Net;
using MyCouch.Requests;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace EMS.CouchDd.Observer.Tests
{
    public class DatabaseHelper : IDisposable
    {
        public IMyCouchClient _client { get; set; }

        public async static Task<DatabaseHelper> Create(string baseUrl, string database, string username, string password)
        {
            DbConnectionInfo connectionInfo = new DbConnectionInfo(baseUrl, database);
            connectionInfo.BasicAuth = new BasicAuthString(username, password);

            var helper =  new DatabaseHelper(new MyCouchClient(connectionInfo));

            await helper._client.Database.PutAsync();

            return helper;
        }


        public DatabaseHelper(MyCouchClient client)
        {
            _client = client;
        }

        public async Task AddDoc(string doc)
        {            
            var response = await _client.Documents.PostAsync(doc);
        }

        public async void Dispose()
        {
            await _client.Database.DeleteAsync();
            _client = null;
        }
    }
}
