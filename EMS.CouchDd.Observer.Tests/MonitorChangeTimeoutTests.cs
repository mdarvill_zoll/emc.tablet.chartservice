﻿using EMC.CouchDb.Sequence.Observable;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace EMS.CouchDd.Observer.Tests
{
    [TestClass]
    public class MonitorChangeTimeoutTests
    {
        [TestMethod]
        public void TimerTriggersEventWhenPingTimeIsOutOfDate()
        {
            AutoResetEvent restEvent = new AutoResetEvent(false);
            bool eventHasBeenTriggered = false;

            TimeoutHandler handler = () =>
            {
                eventHasBeenTriggered = true;
                restEvent.Set();
            };

            MonitorChangesTimeout monitor = new MonitorChangesTimeout();

            monitor.Ping = DateTime.UtcNow.AddSeconds(-30000);
            monitor.Interval = 5000;
            monitor.PollingTimeOut = 5000;

            monitor.Start(handler);

            restEvent.WaitOne(10000);

            Assert.IsTrue(eventHasBeenTriggered);
        }

        [TestMethod]
        public void TimerDoesNotTriggersEventWhenPingTimeIsCurrent()
        {
            AutoResetEvent restEvent = new AutoResetEvent(false);
            bool eventHasBeenTriggered = false;

            TimeoutHandler handler = () =>
            {
                eventHasBeenTriggered = true;
                restEvent.Set();
            };


            MonitorChangesTimeout monitor = new MonitorChangesTimeout();

            monitor.Ping = DateTime.UtcNow;
            monitor.Interval = 1000;
            monitor.PollingTimeOut = 5000;

            monitor.Start(handler);

            restEvent.WaitOne(1000);

            Assert.IsFalse(eventHasBeenTriggered);
        }
    }
}