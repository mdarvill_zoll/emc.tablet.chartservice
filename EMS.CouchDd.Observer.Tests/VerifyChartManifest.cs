﻿using EMC.CouchDb.Document.V1;
using EMC.Tablet.ChartService.Completion;

namespace EMS.CouchDb.Observer.Tests
{
    public class VerifyChartManifest : IHandleChartManifest
    {
        public string HandlerName { get; set; } = "Verify";

        public void Handler(string seq, ChartManifestDocument manifest)
        {

        }
    }
}
