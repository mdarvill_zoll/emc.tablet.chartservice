﻿using EMC.CouchDb.Sequence.Observable;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.CouchDd.Observer.Tests
{
    [TestClass]
    public class SequenceObserverTests
    {
        [TestMethod]
        public void ChangeObserver_NamePropertyIsPopulated()
        {
            var testName = "Test Name";

            EMC.CouchDb.Sequence.Observable.Observer observer = new EMC.CouchDb.Sequence.Observable.Observer(testName);

            Assert.IsTrue(observer.Name.Contains(testName));
        }

        [TestMethod]
        public void ChangeObserver_ThrowExceptionWhenNameIsEmpty()
        {
            bool exceptionThrown = false;

            try
            {
                EMC.CouchDb.Sequence.Observable.Observer observer = new EMC.CouchDb.Sequence.Observable.Observer(null);
            } 
            catch(ArgumentException ex)
            {
                Assert.IsTrue(ex.ParamName.Contains("name"));
                exceptionThrown = true;
            }

            Assert.IsTrue(exceptionThrown, "ArgumentException.ParamName = name was not thrown.");
        }

        [TestMethod]
        public void ChangeObserver_Register()
        {

        }

        [TestMethod]
        public void ChangeObserver_DisposeRegisters()
        {

        }
    }
}
