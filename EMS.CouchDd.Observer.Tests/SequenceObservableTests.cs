﻿using EMC.CouchDb.Document.V1;
using EMC.CouchDb.Sequence.Observable;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.CouchDd.Observer.Tests
{
    [TestClass]
    public class SequenceObservableTests
    {
        [TestMethod]
        public void CreateObservable()
        {
            Mock<IChangesMonitor> moqChanges = new Mock<IChangesMonitor>();
            Observable o = new Observable(moqChanges.Object);
        }

        [TestMethod]
        public void Subscribe()
        {
            Mock<IChangesMonitor> moqChanges = new Mock<IChangesMonitor>();
            Mock<IObserver<SequenceDocument>> moqHandler = new Mock<IObserver<SequenceDocument>>();

            var observable = new Observable(moqChanges.Object);
            var unsubscriber = observable.Subscribe(moqHandler.Object);

            Assert.IsNotNull(unsubscriber);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SubscribeCannotBeNull()
        {
            Mock<IChangesMonitor> moqChanges = new Mock<IChangesMonitor>();

            var observable = new Observable(moqChanges.Object);
            observable.Subscribe(null);
        }

        [TestMethod]
        public void SubscribeCannotBeAddedMoreThanOnce()
        {
            Mock<IChangesMonitor> moqChanges = new Mock<IChangesMonitor>();
            Mock<IObserver<SequenceDocument>> moqHandler = new Mock<IObserver<SequenceDocument>>();

            var observable = new Observable(moqChanges.Object);
            var unsubscriber = observable.Subscribe(moqHandler.Object);

            Assert.IsNotNull(unsubscriber);

            var exceptionThrown = false;
            try
            {
                observable.Subscribe(moqHandler.Object);
            } 
            catch(ArgumentNullException)
            {
                exceptionThrown = false;
            }
            catch
            {
                exceptionThrown = true;
            }

            Assert.IsTrue(exceptionThrown, "No Exception throw as expected");
        }

        [TestMethod]
        public void UnsubscribeRemoved()
        {
            Mock<IChangesMonitor> moqChanges = new Mock<IChangesMonitor>();
            Mock<IObserver<SequenceDocument>> moqHandler = new Mock<IObserver<SequenceDocument>>();

            var observable = new Observable(moqChanges.Object);
            var unsubscriber = observable.Subscribe(moqHandler.Object);
            
            Assert.IsNotNull(unsubscriber);

            //TODO: cannot see into the unsubscriber to verify that it was actually removed.
            unsubscriber.Dispose();
        }

        [TestMethod]
        public void NotifyChangeDoesNotErrorWhenThereAreNoSubscribers()
        {
            Mock<IChangesMonitor> moqChanges = new Mock<IChangesMonitor>();

            var observable = new Observable(moqChanges.Object);

            JObject seqDoc = JObject.FromObject(new { Id = "test" });

            observable.NotifyChange(seqDoc.ToString());
        }

        [TestMethod]
        public void OnNextSingleObserverTriggers()
        {
            Mock<IChangesMonitor> moqChanges = new Mock<IChangesMonitor>();
            Mock<IObserver<SequenceDocument>> moqHandler = new Mock<IObserver<SequenceDocument>>();

            var observable = new Observable(moqChanges.Object);
            var unsubscriber = observable.Subscribe(moqHandler.Object);

            Assert.IsNotNull(unsubscriber);

            JObject seqDoc = JObject.FromObject(new { Id = "test" });

            observable.NotifyChange(seqDoc.ToString());

            moqHandler.Verify(m => m.OnNext(It.IsAny<SequenceDocument>()), Times.Once);
        }

        [TestMethod]
        public void OnNextMultipleObserversTriggers()
        {
            Mock<IChangesMonitor> moqChanges = new Mock<IChangesMonitor>();
            Mock<IMonitorChangesTimeout> moqTimer = new Mock<IMonitorChangesTimeout>();
            Mock<IObserver<SequenceDocument>> moqHandler1 = new Mock<IObserver<SequenceDocument>>();
            Mock<IObserver<SequenceDocument>> moqHandler2 = new Mock<IObserver<SequenceDocument>>();

            var observable = new Observable(moqChanges.Object);
            var unsubscriber1 = observable.Subscribe(moqHandler1.Object);
            var unsubscriber2 = observable.Subscribe(moqHandler2.Object);

            Assert.IsNotNull(unsubscriber1);
            Assert.IsNotNull(unsubscriber2);

            JObject seqDoc = JObject.FromObject(new { Id = "test" });

            observable.NotifyChange(seqDoc.ToString());

            moqHandler1.Verify(m => m.OnNext(It.IsAny<SequenceDocument>()), Times.Once);
            moqHandler2.Verify(m => m.OnNext(It.IsAny<SequenceDocument>()), Times.Once);
        }

        [TestMethod]
        public void OnNextOnUnsubscribedTriggerDoesNotFire()
        {
            Mock<IChangesMonitor> moqChanges = new Mock<IChangesMonitor>();
            Mock<IObserver<SequenceDocument>> moqHandler1 = new Mock<IObserver<SequenceDocument>>();
            Mock<IObserver<SequenceDocument>> moqHandler2 = new Mock<IObserver<SequenceDocument>>();

            var observable = new Observable(moqChanges.Object);
            var unsubscriber1 = observable.Subscribe(moqHandler1.Object);
            var unsubscriber2 = observable.Subscribe(moqHandler2.Object);

            Assert.IsNotNull(unsubscriber1);
            Assert.IsNotNull(unsubscriber2);

            JObject seqDoc = JObject.FromObject(new { Id = "test" });

            observable.NotifyChange(seqDoc.ToString());

            moqHandler1.Verify(m => m.OnNext(It.IsAny<SequenceDocument>()), Times.Once);
            moqHandler2.Verify(m => m.OnNext(It.IsAny<SequenceDocument>()), Times.Once);

            unsubscriber2.Dispose();

            observable.NotifyChange(seqDoc.ToString());

            moqHandler1.Verify(m => m.OnNext(It.IsAny<SequenceDocument>()), Times.Exactly(2));
            moqHandler2.Verify(m => m.OnNext(It.IsAny<SequenceDocument>()), Times.Once);

        }
    }
}
