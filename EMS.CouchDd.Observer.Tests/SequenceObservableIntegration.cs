﻿using AutoFixture;
using EMC.CouchDb.Document.V1;
using EMC.CouchDb.Sequence.Observable;
using EMC.Tablet.ChartService.Common;
using EMC.Tablet.Documents.V1;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace EMS.CouchDd.Observer.Tests
{
    [TestClass]
    public class SequenceObservableIntegration
    {
        Fixture _fixture = new Fixture();
        Configuration config;
        MasterConnection masterConfig;

        public SequenceObservableIntegration()
        {
            config = new Configuration();
            config.Load(JObject.FromObject(new {
                completionDatabase = JObject.FromObject(new {

                    BaseUrl = "http://a954c9f26509.ngrok.io ",
                    Database = "completiondb",
                    Password = "!Password1",
                    Username = "administrator",
                    PollingWait = 30
                }),
                userDatabase = JObject.FromObject(new
                {
                    BaseUrl = "http://a954c9f26509.ngrok.io ",
                    DatabasePrefix = "um_",
                    Password = "!Password1",
                    Username = "administrator",
                    PollingWait = 30
                })
            }));

            masterConfig = config.GetSection<MasterConnection>(Configuration.SectionCompletionDatabase);
        }

        [TestMethod]
        public async Task SequenceObservable_DetectsChange()
        {
            using (var a = await DatabaseHelper.Create(masterConfig.BaseUrl, masterConfig.Database, masterConfig.Username, masterConfig.Password))
            {

                string view = "{\"_id\": \"_design/newDesignDoc\", \"views\": {\"new-view\": {\"map\": \"function (doc) {  if (doc.type === 'LockingDoc') { emit(doc.createDateTime, doc); }}\"}},\"language\": \"javascript\"}";
                await a.AddDoc(view);

                IChangesMonitor change = new ChangesMonitor(config);

                AutoResetEvent autoReset = new AutoResetEvent(false);
                string seqRecieved = string.Empty;

                change.Register((string seq) =>
                {
                    autoReset.Set();
                    seqRecieved = seq;
                });

                Observable observer = new Observable(change);
                observer.Start();

                ChartManifestDocument manifest = new ChartManifestDocument
                {
                    State = "ready",
                    Id = "ChartManifestDocument_Test",
                    Manifest = new List<ChartManifest>
                    {
                        new ChartManifest {Revision = "1234", DocId = "id"}
                    }
                };
                await a.AddDoc(manifest.ToStringObject());


                autoReset.WaitOne(10000);

                Assert.IsTrue(!string.IsNullOrEmpty(seqRecieved));

                JObject jobject = JObject.Parse(seqRecieved);
                SequenceDocument doc = JsonConvert.DeserializeObject<SequenceDocument>(jobject.ToString());

                Assert.IsNotNull(doc);
                Assert.IsNotNull(doc.Id);
                Assert.IsNotNull(doc.Seq);
                Assert.IsNotNull(doc.Doc);
                Assert.IsNotNull(doc.Changes);
            }
        }

        [TestMethod]
        public async Task SequenceObserver()
        {
            using (var a = await DatabaseHelper.Create(masterConfig.BaseUrl, masterConfig.Database, masterConfig.Username, masterConfig.Password))
            {

                string view = "{\"_id\": \"_design/newDesignDoc\", \"views\": {\"new-view\": {\"map\": \"function (doc) {  if (doc.type === 'LockingDoc') { emit(doc.createDateTime, doc); }}\"}},\"language\": \"javascript\"}";
                await a.AddDoc(view);

                //ILockChanges locker = new LockChanges(config.CreateCouchClient());
                Observable observable = new Observable(config);
                EMC.CouchDb.Sequence.Observable.Observer observer = new EMC.CouchDb.Sequence.Observable.Observer("test observer");
                SequenceHandler handler = new SequenceHandler();
                handler.HandlerName = "ready";

                observer.Register(handler);

                var disposable = observable.Subscribe(observer);
                observable.Start();

                long PatientReportId = _fixture.Create<long>();
                int serviceId = _fixture.Create<int>();
                int userId = _fixture.Create<int>();

                ChartManifestDocument manifest = new ChartManifestDocument
                {

                    State = "Ready",
                    ChartActivity = "Upload",
                    Id = $"ChartManifest_{userId}_{serviceId}_{PatientReportId}",
                    ServiceId = serviceId.ToString(),
                    UserId = userId.ToString(),
                    PatientReportId = PatientReportId.ToString(),
                    Manifest = new List<ChartManifest>
                    {
                        new ChartManifest {Revision = "1_" + _fixture.Create<string>(), DocId = _fixture.Create<string>()},
                        new ChartManifest {Revision = "1_" + _fixture.Create<string>(), DocId = _fixture.Create<string>()},
                        new ChartManifest {Revision = "1_" + _fixture.Create<string>(), DocId = _fixture.Create<string>()},
                        new ChartManifest {Revision = "1_" + _fixture.Create<string>(), DocId = _fixture.Create<string>()}
                    },
                    Messages = new List<ChartMessage>
                    {
                        new ChartMessage {Code = "MOB" + _fixture.Create<int>(),  Message = "CCDuration is missing."},
                        new ChartMessage {Code = "MOB" + _fixture.Create<int>(),  Message = "CCDuration was expected to be a number value."},
                        new ChartMessage {Code = "MOB" + _fixture.Create<int>(),  Message = "Detected a patient but could not find a patient document for this chart."},
                        new ChartMessage {Code = "MOB" + _fixture.Create<int>(),  Message = "Oh no!  Something happened and it used to work, please call support."},
                    }
                };
                await a.AddDoc(manifest.ToStringObject());

                int iTry = 0;
                do
                {
                    Thread.Sleep(1000);
                    iTry++;
                } while (iTry <= 10 && handler.Doc == null);

                Assert.IsNotNull(handler.Doc);

                SequenceDocument doc = handler.Doc;

                Assert.IsNotNull(doc);
                Assert.IsNotNull(doc.Id);
                Assert.IsNotNull(doc.Seq);
                Assert.IsNotNull(doc.Doc);
                Assert.IsNotNull(doc.Changes);
            }
        }

        public class SequenceHandler : ISequenceHandler
        {
            public SequenceHandler()
            { }


            public SequenceDocument Doc{get; set;}
            public string HandlerName { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

            public void Handle(SequenceDocument message)
            {
                Doc = message;
            }
        }
    }
}