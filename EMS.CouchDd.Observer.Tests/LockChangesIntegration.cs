﻿using AutoFixture;
using EMC.CouchDb.Sequence.Observable;
using EMC.Tablet.ChartService.Common;
using EMC.Tablet.Documents.V1;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace EMS.CouchDd.Observer.Tests
{
    [TestClass]
    public class LockChangesIntegration
    {
        Fixture _fixture = new Fixture();

        [TestMethod]
        public async Task LockDocument_LocksOnceAdded()
        {

            Configuration config = new Configuration
            {
                BaseUrl = "http://00f038e9b7e8.ngrok.io",
                Database = "notify_db",
                Password = "!Password1",
                Username = "administrator",
            };

            LockChanges lockChanges = new LockChanges(config.CreateCouchClient());

            LockDocument doc = new LockDocument(DocumentTypes.LockingDoc, "Ready", _fixture.Create<long>().ToString(), 0);

            Assert.IsFalse(await lockChanges.IsChangeLocked(doc));

            Assert.IsTrue(await lockChanges.IsChangeLocked(doc));
        }

        [TestMethod]
        public async Task Test()
        {
            Configuration config = new Configuration
            {
                BaseUrl = "http://00f038e9b7e8.ngrok.io",
                Database = "notify_db",
                Password = "!Password1",
                Username = "administrator",
            };

            LockChanges lockChanges = new LockChanges(config.CreateCouchClient());

           // LockDocument doc = new LockDocument(DocumentTypes.LockingDoc, "Ready", "12334", 0);

            var a = await lockChanges.GetLastSequence();
        }
    }
}
