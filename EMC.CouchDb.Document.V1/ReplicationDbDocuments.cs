﻿using Newtonsoft.Json.Linq;

namespace EMC.CouchDb.Document.V1
{
    public class ReplicationDbDocuments
    {
        public static JObject GetDesighTestDoc()
        {
            return JObject.FromObject(new
            {
                _id = "_design/test",
                filters = JObject.FromObject(new
                {
                    chart_complete = "function(doc, req) { if (doc.doc_type === 'ChartManifest' && doc.state === 'chart-complete' && doc.version === 'EMC.CouchDb.Document.V1') { return true; } else { return false; }}",
                    chart_upload = "function(doc, req) { if (doc.doc_type === 'ChartManifest' && doc.state === 'chart-upload' && doc.version === 'EMC.CouchDb.Document.V1') { return true; } else { return false; }}",
                    chart_delete = "function(doc, req) { if (doc.doc_type === 'ChartManifest' && doc.state === 'chart-delete' && doc.version === 'EMC.CouchDb.Document.V1') { return true; } else { return false; }}",
                    chart_notify = "function(doc, req) { if (doc.doc_type === 'ChartManifest' && doc.state === 'chart-notify' && doc.version === 'EMC.CouchDb.Document.V1') { return true; } else { return false; }}"
                })
            });
        }
    }
}
