﻿using EMC.Tablet.Documents.V1;
using System;
using System.Collections.Generic;

namespace EMC.CouchDb.Document.V1
{
    public class ChartManifestDocument : BaseDocument
    {
        public ChartManifestDocument() : base(DocumentTypes.ChartManifest)
        {
            _version = typeof(ChartManifestDocument).Namespace.ToString();
        }

        public long RetryInSeconds { get; set; }

        public string PatientReportId { get; set; }

        public string UserId { get; set; }

        public string ServiceId { get; set; }

        public string State { get; set; }

        public string ChartActivity { get; set; }

        public int ErrorRetries { get; set; } = 0;

        public IList<ChartManifest> Manifest { get; set; }

        public IList<ChartMessage> Messages { get; set; }

        public DateTime ModifiedTime { get; set; }

        public bool Ignore { get; set; }

        public bool TraceOn { get; set; }

        public IList<ChartTrace> Trace { get; set; }
    }
}
