﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMC.CouchDb.Document.V1
{
    public class ChartMessage
    {
        public string Code { get; set; }

        public string Message { get; set; }
    }
}
