﻿using System;
namespace EMC.Tablet.Documents.V1
{
    public enum DocumentTypes
    {
        None,
        LockingDoc,
        ChartManifest,
        ServiceReport
    }
}
