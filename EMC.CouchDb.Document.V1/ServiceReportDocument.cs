﻿using EMC.Tablet.Documents.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMC.CouchDb.Document.V1
{
    public class ServiceReportDocument : BaseDocument
    {
        public ServiceReportDocument() : base(DocumentTypes.ServiceReport)
        {
            _version = typeof(ChartManifestDocument).Namespace.ToString();
        }

        public IList<Service> Services {get; set;}

    }

    public class Service
    {
        public string AppnName { get; set; }

        public string AppVersion { get; set; }

        public IList<string> Dlls { get; set; }

        public DateTime LastModifiedTime { get; set; }
    }
}
