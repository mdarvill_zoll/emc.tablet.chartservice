﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMC.CouchDb.Document.V1
{
    public class SequenceDocument
    {
        public string Seq { get; set; }

        public string Id { get; set; }

        public bool Deleted { get; set; }

        public JObject Doc {get; set;}

        public Change[] Changes { get; set; }
    }

    public class Change {
        public string Rev { get; set; }

        public bool Deleted { get; set; }
    }
}
