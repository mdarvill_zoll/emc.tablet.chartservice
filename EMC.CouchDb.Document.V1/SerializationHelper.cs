﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace EMC.Tablet.Documents.V1
{
    public static class SerializationHelper
    {
        public static string GetDocumentTypeValueString(this DocumentTypes documentType)
        {
            return Enum.GetName(typeof(DocumentTypes), documentType);
        }

        public static bool Compare(this JToken documentType, DocumentTypes to)
        {
            if (documentType == null)
                return false;

            return documentType.ToString() == Enum.GetName(typeof(DocumentTypes), (int)to);
        }

        public static string ToStringObject(this object o)
        {
            var serializerSettings = new Newtonsoft.Json.JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore
            };

            return JsonConvert.SerializeObject(o, serializerSettings);
        }

        public static JObject ToJObject(this object o)
        {
            var jsonSerializer = new JsonSerializer
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore
            };

            return JObject.FromObject(o, jsonSerializer);
        }
    }
}
