﻿using System;

namespace EMC.Tablet.Documents.V1
{
    public class LockDocument : BaseDocument
    {
        public LockDocument(DocumentTypes docType, string state, string sequenceId, int attempt): base(docType)
        {
            _version = typeof(LockDocument).Namespace.ToString();

            Id = $"{docType.GetDocumentTypeValueString()}_{state}_{sequenceId}_{attempt}";
            Type = docType;

            SequenceNumber = sequenceId;
            CreateDateTime = DateTime.UtcNow;
        }

        public string DocId { get; set; }

        public string State { get; set;}
        
        public string SequenceNumber { get; set; }

        public DateTime CreateDateTime { get; set; }
    }
}
