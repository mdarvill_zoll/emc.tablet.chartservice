using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EMC.Tablet.Documents.V1
{
    public class BaseDocument
    {
        protected string _version;

        public BaseDocument(DocumentTypes docType)
        {
            Type = docType;
        }

        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "_rev")]
        public string Revision { get; set; }

        public string Version { get { return _version; } }

        [JsonProperty(PropertyName = "type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DocumentTypes Type { get; set; }
    }
}