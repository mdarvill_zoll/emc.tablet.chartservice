﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMC.CouchDb.Document.V1
{
    public class ChartManifest
    {
        public string DocId { get; set; }

        public string Revision { get; set; }
    }
}
