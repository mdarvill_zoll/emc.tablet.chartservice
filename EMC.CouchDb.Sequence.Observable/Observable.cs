﻿using EMC.CouchDb.Document.V1;
using EMC.Tablet.ChartService.Common;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace EMC.CouchDb.Sequence.Observable
{
    public class Observable : IObservable<SequenceDocument>
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(SequenceDocument));
        private readonly IChangesMonitor _changes;
        private readonly List<IObserver<SequenceDocument>> observers = new List<IObserver<SequenceDocument>>();
        private readonly Configuration _config;

        public Observable(IChangesMonitor changes)
        {
            _changes = changes ?? throw new ArgumentNullException(nameof(changes));
        }

        public Observable(Configuration config)
        {
            _config = config ?? throw new ArgumentNullException(nameof(config));

            _changes = new ChangesMonitor(_config);
            _changes.Register(NotifyChange);
        }

        public void Start()
        {
            _changes.Listener();
        }

        public IDisposable Subscribe(IObserver<SequenceDocument> observer)
        {
            if (observer == null)
                throw new ArgumentNullException(nameof(observer));

            if (observers.Contains(observer))
                throw new Exception($"{observer} already exists");

             observers.Add(observer);
             return new ObserverUnsubscriber<SequenceDocument>(observers, observer);
        }

        public void NotifyChange(string seq)
        {
            _log.Debug($"Starting NotifyChange change.");

            if (string.IsNullOrWhiteSpace(seq))
                return;

            try
            {
                _log.Debug("NotifyChange: deserializing SequenceDocument.");
                JObject seqObj = JObject.Parse(seq);
                SequenceDocument seqDoc = JsonConvert.DeserializeObject<SequenceDocument>(seqObj.ToString());

                if (seqDoc.Deleted)
                {
                    _log.Debug($"Stopping NotifyChange: This sequence {seqDoc.Seq} has been deleted.");
                    return;
                }

                foreach (var observer in observers)
                    observer.OnNext(seqDoc);
            } 
            catch(Exception ex)
            {
                _log.Warn("Stopping NotifyChange: Unknown Exception.", ex);
            }
        }
    }
}