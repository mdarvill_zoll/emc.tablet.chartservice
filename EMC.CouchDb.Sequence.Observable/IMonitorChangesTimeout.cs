﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace EMC.CouchDb.Sequence.Observable
{
    public interface IMonitorChangesTimeout
    {
        bool Touch { get; set; }

        void Start(TimeoutHandler timeoutHandler);

        void Stop();

        public long IntervalTimeOut { get; set; }
    }

    public delegate void TimeoutHandler();
}
