﻿using EMC.CouchDb.Document.V1;

namespace EMC.CouchDb.Sequence.Observable
{
    public interface ISequenceHandler
    {
        string HandlerName { get; set; }

        void Handle(SequenceDocument message);
    }
}
