﻿using EMC.Tablet.ChartService.Common;
using log4net;
using MyCouch;
using MyCouch.Requests;
using System;
using System.Threading;

namespace EMC.CouchDb.Sequence.Observable
{
    public class ChangesMonitor : IChangesMonitor
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ChangesMonitor));
        private readonly IMonitorChangesTimeout _timer;
        private readonly IMyCouchClient _couchClient;
        private ChangeHandler _changeHandler;
        private CancellationTokenSource _tokenSource;
        private ILockChanges _lockChanges;
        private MasterConnection _masterConnection;

        public ChangesMonitor( IMyCouchClient couchClient, IMonitorChangesTimeout timer, ILockChanges lockChanges, MasterConnection masterConnection)
        {
            _masterConnection = masterConnection ?? throw new ArgumentNullException(nameof(masterConnection));
            _couchClient = couchClient ?? throw new ArgumentNullException(nameof(couchClient));
            _timer = timer ?? throw new ArgumentNullException(nameof(timer));
            _lockChanges = lockChanges ?? throw new ArgumentNullException(nameof(lockChanges));

            _tokenSource = new CancellationTokenSource();
        }

        public ChangesMonitor(Configuration config)
        {
            if(config == null)
                throw new ArgumentNullException(nameof(config));

            _masterConnection = config.GetSection<MasterConnection>(Configuration.SectionCompletionDatabase);
            _tokenSource = new CancellationTokenSource();
            _couchClient  = _masterConnection.Create();

            var observerSection = config.GetSection<SequenceObserverSection>(Configuration.SectionSequenceObserver);
            
            _timer = new MonitorChangesTimeout();
            _timer.IntervalTimeOut = observerSection.IntervalTimeOut;



            _lockChanges = new LockChanges(_couchClient);
        }

        public void Register(ChangeHandler changeHandler)
        {
            if (changeHandler == null)
                throw new ArgumentNullException(nameof(changeHandler), $"Register requires that the property {nameof(changeHandler)} not be null.");

            _changeHandler += changeHandler;
        }

        public void Unregister(ChangeHandler changeHandler)
        {
            if (changeHandler == null)
                throw new ArgumentNullException(nameof(changeHandler), $"Unregister requires that the property {nameof(changeHandler)} not be null.");

            _changeHandler -= changeHandler;
        }

        public async void Listener()
        {
            try
            {
                _log.Info("A listener is starting...");
                if (_tokenSource.IsCancellationRequested)
                    return;

                _log.Debug("Finding the last sequence number we remember.");
                string sequence = await _lockChanges.GetLastSequence();
                _log.Info($"The last sequence is {sequence}, restarting from there.");


                _log.Debug("Listener: Starting the timeout timer.");
                _timer.Start(OnTimedOut);

                GetChangesRequest request = new GetChangesRequest
                {
                    Feed = ChangesFeed.Continuous,
                    IncludeDocs = true,
                    Heartbeat = _masterConnection.Heartbeat,
                    Since = sequence
                };

                if (!string.IsNullOrEmpty(_masterConnection.ChangeFilter))
                {
                    _log.Debug($"Listener: Add a filter to the change feed.");
                    request.Filter = _masterConnection.ChangeFilter;
                }
                else
                {
                    _log.Debug($"Listener: A filter is not present.");
                }

                _log.Debug($"Listener: Starting the change feed on database '{_couchClient.Connection.DbName}'.");
                _couchClient.Changes.GetAsync(request, (data) =>
                {
                    OnChange(data);
                }, _tokenSource.Token);
            } 
            catch(Exception ex)
            {
                _log.Error($"Something has happened that was not expected while creating the listener.", ex);
            }
        }

        public async void OnTimedOut()
        {
            try
            {
                _log.Info("OnTimedOut: The timer has detected that the listener may have been stopped.");
                _timer.Stop();

                _log.Info("OnTimedOut: Canceling the current listener just in case we are wrong.");
                _tokenSource.Cancel();

                _tokenSource = new CancellationTokenSource();

                _log.Debug("OnTimedOut: Restarting the listener.");
                Listener();
            } 
            catch (Exception ex)
            {
                _log.Error("OnTimedOut: While handling OnTimedOut an unexpected exception was handled.", ex);
                _log.Error("OnTimedOut: The application will try again later.");
            }
        }

        public void OnChange(string seq)
        {
            _timer.Touch = true;

            _log.Debug("A new change has been detected!");

            _changeHandler?.Invoke(seq);
        }
    }
}
