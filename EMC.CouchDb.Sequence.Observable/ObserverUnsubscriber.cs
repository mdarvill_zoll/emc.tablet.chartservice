﻿using log4net;
using System;
using System.Collections.Generic;

namespace EMC.CouchDb.Sequence.Observable
{
    public sealed class ObserverUnsubscriber<SequenceDocument> : IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ObserverUnsubscriber<SequenceDocument>));
        private List<IObserver<SequenceDocument>> _observers;
        private IObserver<SequenceDocument> _observer;

        public ObserverUnsubscriber(List<IObserver<SequenceDocument>> observers, IObserver<SequenceDocument> observer)
        {
            _log.Debug($"Adding observer {observer}");
            this._observers = observers;
            this._observer = observer;
        }

        public void Dispose()
        {
            _log.Debug($"Disposing of observer {_observer}");
            if (_observers.Contains(_observer))
            {
                _observers.Remove(_observer);
            }
        }
    }
}
