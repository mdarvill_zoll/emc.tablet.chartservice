﻿using EMC.CouchDb.Document.V1;
using EMC.Tablet.Documents.V1;
using log4net;
using System;
using System.Collections.Generic;

namespace EMC.CouchDb.Sequence.Observable
{
    public class Observer : IObserver<SequenceDocument>
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Observer));
        private readonly Dictionary<string, ISequenceHandler> _handlers; 
        private IDisposable _unsubscriber;

        public string Name { get; set; } = "<Undefined>";

        public void Register(ISequenceHandler handler)
        {
            _log.Debug($"Register {Name}: Add new sequence hander.");
            _handlers.Add(handler.HandlerName, handler);
        }

        public Observer(string name)
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name), "The observer must be assigned a name.");

            this.Name = name;
            _handlers = new Dictionary<string, ISequenceHandler>();
        }

        public virtual void Subscribe(Observable provider)
        {
            _unsubscriber = provider.Subscribe(this);
        }

        public virtual void Unsubscribe()
        {
            _unsubscriber.Dispose();
            _handlers.Clear();
        }

        public virtual void OnCompleted()
        {
            _log.Debug($"OnCompleted {Name}: I am done listening for new Observable changes.");
        }

        // No implementation needed: Method is not called by the BaggageHandler class.
        public virtual void OnError(Exception e)
        {
            _log.Debug($"OnError {Name}: An unexpected error has been caught.", e);
        }

        // Update information.
        public virtual void OnNext(SequenceDocument sequenceDocument)
        {
            _log.Debug($"Starting OnNext {Name}:  Starting OnNext.");

            //todo this should be in the handler method and not here since the type may change between documents
            if (sequenceDocument?.Doc != null && SerializationHelper.Compare(sequenceDocument.Doc["type"], DocumentTypes.ChartManifest))
            {
                _log.Debug($"OnNext {Name}:  A SequenceDocument for {sequenceDocument.Doc["type"]} has been detected.");

                var state = (string)sequenceDocument.Doc["state"];

                if (!string.IsNullOrEmpty(state) && _handlers.ContainsKey(state))
                {
                    _log.Debug($"OnNext {Name}:  Triggering the handler for state {state}.");
                    _handlers[state].Handle(sequenceDocument);
                }
                else
                {
                    _log.Debug($"Stopping OnNext {Name}:  This {sequenceDocument.Doc["type"]} is not supported for on next.");
                }
            }
        }
    }
}
