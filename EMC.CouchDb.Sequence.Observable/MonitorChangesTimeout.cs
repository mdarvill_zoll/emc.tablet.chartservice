﻿using log4net;
using System;
using System.Timers;
using token = System.Threading;

namespace EMC.CouchDb.Sequence.Observable
{
    public sealed class MonitorChangesTimeout: IMonitorChangesTimeout, IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(IMonitorChangesTimeout));
        private TimeoutHandler _timeoutHandler;
        private Timer _timer;
        private token.CancellationTokenSource _cancellationTokenSource;

        public MonitorChangesTimeout()
        {
            _cancellationTokenSource = new token.CancellationTokenSource();
        }

        public bool Touch { get; set; }

        public long IntervalTimeOut { get; set; } = 30000;

        public Timer GetTimer()
        {
            _timer = new Timer
            {
                AutoReset = true,
                Interval = IntervalTimeOut
            };

            _timer.Elapsed += new ElapsedEventHandler(OnPingCheck);

            return _timer;
        }

        public void Start(TimeoutHandler timeoutHandler/* start or restart the change feed*/)
        {
            if(timeoutHandler == null)
                 new ArgumentNullException(nameof(timeoutHandler));

            _timeoutHandler += timeoutHandler;

            GetTimer().Start();
        }

        public void Stop()
        {
            GetTimer().Stop();

            _timeoutHandler = null;
        }

        public void OnPingCheck(object sender, ElapsedEventArgs args)
        {
            if (Touch)
            {
                _log.Info("Polling check: no timeout detected.");
                Touch = false;
                return;
            }

            _log.Info("Polling check, wait, what? what!? Our changefeed appears to have timed out.");
            _timeoutHandler?.Invoke();
        }

        public void Dispose()
        {
            if (_timer != null)
            {
                _timer.Stop();
                _timer.Dispose();
            }
               
        }
    }
}
