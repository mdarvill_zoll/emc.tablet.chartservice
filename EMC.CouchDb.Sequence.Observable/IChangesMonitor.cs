﻿using EMC.CouchDb.Document.V1;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMC.CouchDb.Sequence.Observable
{
    public interface IChangesMonitor
    {
        void Listener();

        void OnChange(string seq);

        void OnTimedOut();

        void Register(ChangeHandler changeHandler);

        void Unregister(ChangeHandler changeHandler);
    }

    public delegate void ChangeHandler(string seq);
}
