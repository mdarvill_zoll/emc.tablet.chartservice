﻿using EMC.CouchDb.Sequence.Observable;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using EMC.Tablet.ChartService.Completion;

namespace EMC.Tablet.ChartCompleteService
{
    public class ServiceController : IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ServiceController));
        private Task _currentRun;
        private HostBuilder _builder;
        private CancellationTokenSource _tokenSource;

        public ServiceController(CancellationTokenSource tokenSource)
        {
            _builder = new HostBuilder();
            _tokenSource = tokenSource ?? throw new ArgumentNullException(nameof(tokenSource));
        }

        internal ServiceController(HostBuilder hostBuilder, CancellationTokenSource tokenSource)
        {
            _builder = hostBuilder ?? throw new ArgumentNullException(nameof(hostBuilder), $"The {nameof(hostBuilder)} cannot be null");
            _tokenSource = tokenSource ?? throw new ArgumentNullException(nameof(tokenSource));
        }

        public void Configure()
        {
            XmlDocument log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead("log4net.config"));

            var repo = log4net.LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));

            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
        }

        public void Initialize()
        {
            //var loader = new Configuration("EMC.Tablet.ChartCompleteService.json");
    
            _log.Info("Initializing services ....");
            CompletionServiceRunner completionRunner = new CompletionServiceRunner();
            completionRunner.Initialize(_tokenSource.Token);

            _log.Info("Building");
            _builder
                .ConfigureServices((services) =>
                {
                    services.AddSingleton(completionRunner);
                });

        }

        public Task StartConsole()
        {
            _log.Info("Starting the host console ....");
            return _currentRun = _builder.RunConsoleAsync(_tokenSource.Token);
        }

        public void Stop()
        {
            _log.Info("Stopping the console ....");
            _tokenSource.Cancel();
        }

        public void Dispose()
        {
            _log.Info("Disposing the console ....");
            if (!_tokenSource.IsCancellationRequested)
                Stop();

            if (_currentRun != null)
            {
                _log.Info("Waiting for the builder to stop.");
                _currentRun.Wait();

                _log.Info("Cleaning up the builder");
                _currentRun.Dispose();
            }

            if (_tokenSource != null)
            {
                _log.Info("Disposing the token");
                _tokenSource.Dispose();
            }

            _builder = null;
        }
    }
}
