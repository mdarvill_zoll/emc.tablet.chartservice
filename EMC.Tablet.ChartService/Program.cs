﻿using log4net;
using MyCouch;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace EMC.Tablet.ChartCompleteService
{
    class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var isService = !(Debugger.IsAttached || args.Contains("--console"));

            CancellationTokenSource cancellation = new CancellationTokenSource();

            using (var controller = new ServiceController(cancellation))
            {
                _log.Info($"Starting {assembly.GetName()}, {FileVersionInfo.GetVersionInfo(assembly.Location)}");
                controller.Configure();

                try
                {
                    controller.Initialize();

                    Task hostTask;

                    if (isService)
                    {
                        _log.Info("Running in release mode.");
                        hostTask = controller.StartConsole(); //todo: this should start program as a service
                        MainConsoleLoop(cancellation);
                        controller.Stop();
                    }
                    else
                    {
                        _log.Info("Running in debug mode.");
                        hostTask = controller.StartConsole();
                        MainConsoleLoop(cancellation);
                        controller.Stop();
                    }

                    hostTask.Wait();
                }
                catch (Exception ex)
                {
                    _log.Error("Error during start up.", ex);
                    _log.Info("Shutting down");
                }
            }
        }

        private static void MainConsoleLoop(CancellationTokenSource cancellation)
        {
            bool exitConsole = false;
            do
            {
                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "exit":
                        exitConsole = true;
                        Console.WriteLine("Stopping host....");
                        cancellation.Cancel();
                        break;
                    default:
                        Console.WriteLine($"Your entry '{choice}' is invalid.");
                        Console.WriteLine("");
                        Console.WriteLine("Type <exit> to shutdown the console.");
                        Console.WriteLine("Type <logTest> to run logging test.");
                        break;
                }
            } while (!exitConsole);
        }
    }
}
