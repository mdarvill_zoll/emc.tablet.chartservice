﻿using EMC.Tablet.Documents.V1;
using System.Threading.Tasks;

namespace EMC.Tablet.ChartService.Common
{
    public interface ILockChanges
    {
        public string ChangeDocName { get; set; }

        public Task<bool> IsChangeLocked(LockDocument doc);

        public Task<string> GetLastSequence();
    }
}
