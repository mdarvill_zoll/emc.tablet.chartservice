﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace EMC.Tablet.ChartService.Common
{
    /// <summary>
    /// TODO: this is really in the wrong spot, it belongs on a common library where the server and other services can get at it.
    /// </summary>
    public class Configuration: IConfiguration
    {
        public const string SectionCompletionDatabase = "CompletionDatabase";
        public const string SectionUserDatabase = "UserDatabase";
        public const string SectionSequenceObserver = "SequenceObserver";

        JObject _config;

        public void Load(JObject doc)
        {
            _config = doc;
        }

        public Configuration()
        {

        }

        public Configuration(string fileName)
        {
            _config = Load(fileName);
        }

        public JObject Load(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new ArgumentException(nameof(fileName));

            if (!File.Exists(fileName))
                throw new Exception($"The configuration file {fileName} does not exist");

            return JObject.Parse(File.ReadAllText(fileName));
        }

        public T GetSection<T> (string node)
        {
            return JsonConvert.DeserializeObject<T>(_config[node].ToString());
        }
    }
}
