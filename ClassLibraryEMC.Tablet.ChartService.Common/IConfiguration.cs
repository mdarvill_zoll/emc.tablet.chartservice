﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMC.Tablet.ChartService.Common
{
    public interface IConfiguration
    {
        T GetSection<T>(string fileName);
    }

}
