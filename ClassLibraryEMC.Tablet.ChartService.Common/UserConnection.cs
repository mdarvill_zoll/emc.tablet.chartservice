﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMC.Tablet.ChartService.Common
{
    public class UserConnection
    {
        public string BaseUrl { get; set; }

        public string Extension { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
