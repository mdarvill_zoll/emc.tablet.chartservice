﻿using EMC.Tablet.Documents.V1;
using log4net;
using MyCouch;
using MyCouch.Requests;
using MyCouch.Responses;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Threading.Tasks;

namespace EMC.Tablet.ChartService.Common
{
    public class LockChanges : ILockChanges
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(LockChanges));
        IMyCouchClient _client;

        public string ChangeDocName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public LockChanges(IMyCouchClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<bool> IsChangeLocked(LockDocument doc)
        {
            DocumentHeaderResponse response = await _client.Documents.PostAsync(doc.ToStringObject());

            if (response.IsSuccess)
                return false;
            else if (response.StatusCode == HttpStatusCode.Conflict)
                return true;

            throw new Exception($"IsChangeLocked failed because: {response.Error}");
        }

        public async Task<string> GetLastSequence()
        {
            QueryViewRequest findRequest = new QueryViewRequest("lockingViews", "lockingView")
            {
                Limit = 1,
                Reduce = false,
                Descending = true,
                IncludeDocs = true
            };

            ViewQueryResponse response = await _client.Views.QueryAsync(findRequest);

            try
            {
                if (response.IsSuccess && response.Rows.Length > 0)
                {
                    var sequenceNumber = JObject.Parse(response.Rows[0].IncludedDoc)["sequenceNumber"].Value<string>();
                    return sequenceNumber;
                }
                else
                {
                    _log.Info($"GetLastSequence: No sequence was detected returning the beginning sequence number of 0");
                    return "0";
                }
            }
            catch(Exception ex)
            {
                _log.Error($"GetLastSequence: An unexpected exception has occurred", ex);
                throw ex;
            }
            
        }
    }
}
