﻿using MyCouch;
using MyCouch.Net;
using System;

namespace EMC.Tablet.ChartService.Common
{
    public static class ConnectionHelper
    {
        public static IMyCouchClient Create(this MasterConnection dbInfo)
        {
            if (String.IsNullOrWhiteSpace(dbInfo.BaseUrl))
                throw new ArgumentException($"dbInfo.BaseUrl cannot be null or empty", "dbInfo.BaseUrl");

            if (String.IsNullOrWhiteSpace(dbInfo.Database))
                throw new ArgumentException($"dbInfo.Database cannot be null or empty", "dbInfo.BaseUrl");

            DbConnectionInfo connectionInfo = new DbConnectionInfo(dbInfo.BaseUrl, dbInfo.Database);

            if (String.IsNullOrWhiteSpace(dbInfo.Username) && String.IsNullOrWhiteSpace(dbInfo.Password))
                throw new ArgumentException($"Configuration.Username and Configuration.Password must be provided", "Configuration.Username");


            connectionInfo.BasicAuth = new BasicAuthString(dbInfo.Username, dbInfo.Password);

            return new MyCouchClient(connectionInfo);
        }

        public static IMyCouchClient Create(this UserConnection dbInfo, string userId)
        {
            if (String.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException(nameof(userId));

            if (String.IsNullOrWhiteSpace(dbInfo.BaseUrl))
                throw new ArgumentException($"Configuration.BaseUrl cannot be null or empty", "Configuration.BaseUrl");

            DbConnectionInfo connectionInfo = new DbConnectionInfo(dbInfo.BaseUrl, $"{dbInfo.Extension}_{userId}");

            if (String.IsNullOrWhiteSpace(dbInfo.Username) && String.IsNullOrWhiteSpace(dbInfo.Password))
                throw new ArgumentException($"Configuration.Username and Configuration.Password must be provided", "Configuration.Username");


            connectionInfo.BasicAuth = new BasicAuthString(dbInfo.Username, dbInfo.Password);

            return new MyCouchClient(connectionInfo);
        }
    }
}
