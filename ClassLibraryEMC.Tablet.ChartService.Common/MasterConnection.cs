﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMC.Tablet.ChartService.Common
{
    public class MasterConnection
    {
        public string BaseUrl { get; set; }

        public string Database { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string ChangeFilter { get; set; }

        public int Heartbeat { get; set; } = 30000;
    }
}
