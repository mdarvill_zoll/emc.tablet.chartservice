﻿using EMC.Tablet.Documents.V1;
using MyCouch;
using MyCouch.Requests;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace EMS.CouchDb.Observers
{
    public class ObserverRepo: IDisposable
    {
        public ChangeHandler Handlers { get; set; }
        public IMyCouchClient _couchClient;
        public DateTime Pinged { get; set; }
        public ChangeFeedObserverArgs _config;

        public ObserverRepo(IMyCouchClient couchClient, ChangeFeedObserverArgs config)
        {;
            _config = config;
            _couchClient = couchClient;
        }

        public void StartChangesFeed(CancellationToken token)
        {
            if (Handlers == null)
                throw new Exception("Handles are not set");

            var task = Task.Run(() =>
            {
                if (!token.IsCancellationRequested)
                {
                    GetChangesRequest request = new GetChangesRequest
                    {
                        Feed = ChangesFeed.Continuous,
                        Filter = _config.ChangesFilter,
                        IncludeDocs = true,
                        Heartbeat = 30000,
                    };

                    _couchClient.Changes.GetAsync(request,
                        (data) => { OnChangeDocument(data); },
                        token);
                }
            });

            task.Start();
            //TODO: this task should be global to the class so that it can later be stopped.
        }

        public async void OnChangeDocument(string sequenceString)
        {
            Pinged = DateTime.Now;

            if (string.IsNullOrEmpty(sequenceString))
            {
                //_log.Debug("Heartbeat detected.");
                return;
            }

            var sequenceDoc = JObject.Parse(sequenceString);

            //document is deleted so we are finished.
            if (sequenceDoc["deleted"]?.Value<bool>() == true ||
                string.IsNullOrWhiteSpace(sequenceDoc?["doc"]?.ToString()))
            {
                //_log.Debug("Document changed is deleted. Stopping Changehandler.");
                return;
            }

            var docId = sequenceDoc["doc"]["_id"].ToString();

            var sequenceNumber = sequenceDoc["seq"].Value<string>().Split('-')[0];

            if (string.IsNullOrWhiteSpace(sequenceNumber))
            {
                //_log.Debug($"Sequence number is null. Stopping Changehandler.");
                return;
            }

            if (sequenceDoc["doc"]["doc_type"] == null)
            {
                //_log.Debug("doc_type is a null value. Stopping Changehandler.");
                return;
            }

            //TOFO: define the document we are looking for.
 //           if (!sequenceDoc["doc"]["doc_type"].Compare(DocumentTypes.Chart_Change)) //todo: this should be configurable.
 //           {
 //               //_log.Debug($"doc_type {sequenceDoc["doc"]["doc_type"]} is not valid. Stopping Changehandler.");
 //               return;
 //           }

            if (await SequenceLocked(DocumentTypes.LockingDocType, "", 0))
                return;

            //if (Handlers != null)
            //    Handlers.Handle(this, new EventArgs());
        }


        public async Task<bool> SequenceLocked(DocumentTypes docType, string sequenceId, int attempt)
        {
            try
            {
                var lockDoc = new LockDocument(docType, sequenceId, attempt);
                var response = await _couchClient.Documents.PostAsync(lockDoc.ToString());

                return !response.IsSuccess;
            } 
            catch(Exception ex)
            {
                return false;
            }
        }

        public void Dispose()
        {
            
        }
    }
}
