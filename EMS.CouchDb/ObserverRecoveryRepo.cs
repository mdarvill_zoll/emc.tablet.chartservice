﻿using EMS.CouchDb.Observers;
using System;
using System.Threading;
using System.Timers;
using Timer = System.Timers.Timer;

namespace EMS.CouchDb
{
    public class ObserverRecoveryRepo: IDisposable
    {
        Timer _timer;
        ObserverRepo _observerRepo;
        CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        public ObserverRecoveryRepo(ObserverRepo observerRepo)
        {
            _observerRepo = observerRepo;
        }

        public void Start()
        {
            if (_timer != null)
                return;

            _timer = CreateTimer();

            StartChangeFeed();
        }

        public Timer CreateTimer()
        {
            var _timer = new Timer
            {
                AutoReset = true,
                Interval = 60000
            };

            _timer.Elapsed += new ElapsedEventHandler(CheckLastPingTime);

            return _timer;
        }

        public void CheckLastPingTime(object sender, ElapsedEventArgs args)
        {
            if ((DateTime.Now - _observerRepo.Pinged).TotalMilliseconds < 30000)
                return;

            cancellationTokenSource.Cancel();

            cancellationTokenSource = new CancellationTokenSource();

            StartChangeFeed();
        }

        public void StartChangeFeed()
        {
            if (!cancellationTokenSource.IsCancellationRequested)
                _observerRepo.StartChangesFeed(cancellationTokenSource.Token);
        }

        public void Dispose()
        {
            if (!cancellationTokenSource.IsCancellationRequested)
                cancellationTokenSource.Cancel();
        }
    }
}
