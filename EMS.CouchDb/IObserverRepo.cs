﻿using EMC.Tablet.Documents.V1;
using System.Threading;
using System.Threading.Tasks;

namespace EMS.CouchDb
{
    public interface IObserverRepo
    {
        CancellationTokenSource GetCancellationTokenSource();

        void StartChangesFeed(CancellationToken token);


        void OnChangeDocument(string sequenceString);


        Task<bool> SequenceLocked(DocumentTypes docType, string sequenceId, int attempt);
    }
}
