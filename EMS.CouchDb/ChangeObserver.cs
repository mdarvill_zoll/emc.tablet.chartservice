﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EMS.CouchDb.Observers;
using MyCouch;

namespace EMS.CouchDb
{
    public class ChangeObserver
    {
        CancellationTokenSource _cancellationToKenSource = new CancellationTokenSource();

        IObserverRepo _observerRepo;

        public ChangeObserver(IObserverRepo observerRepo)
        {
            _observerRepo = observerRepo;
        }

        public void Start()
        {
            _observerRepo.StartChangesFeed(_cancellationToKenSource.Token);
        }

        public void Stop()
        {
            if (_cancellationToKenSource != null)
            {
                _cancellationToKenSource.Cancel();
            }
        }
    }
}
