﻿using EMC.Tablet.Documents.V1;
using EMS.CouchDb.Observers;
using MyCouch;
using MyCouch.Requests;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace EMS.CouchDb.ContinuousObserver
{
    public class ContinuousObserverRepo: IObserverRepo
    {
        public ChangeHandler Handler { get; set; }
        public IMyCouchClient _couchClient;
        public DateTime Pinged { get; set; }
        public ChangeFeedObserverArgs _config;

        public ContinuousObserverRepo(IMyCouchClient couchClient, ChangeFeedObserverArgs config)
        {
            _config = config;
            _couchClient = couchClient;
        }

        public CancellationTokenSource GetCancellationTokenSource()
        {
            return new CancellationTokenSource();
        }

        public void StartChangesFeed(CancellationToken token)
        {
            if (Handler == null)
                throw new Exception("Handles are not set");

            if (!token.IsCancellationRequested)
            {
                GetChangesRequest request = new GetChangesRequest
                {
                    Feed = ChangesFeed.Continuous,
                    Filter = _config.ChangesFilter,
                    IncludeDocs = true,
                    Heartbeat = 30000,

                };

                _couchClient.Changes.GetAsync(
                    request,
                    (data) => { OnChangeDocument(data); },
                    token);
            }

            //TODO: this task should be global to the class so that it can later be stopped.
        }

        public void OnChangeDocument(string sequenceString)
        {
            Pinged = DateTime.Now;

            if (Handler != null)
            {
                Handler.Handle(sequenceString);
            }
            /*
if (string.IsNullOrEmpty(sequenceString))
{
   return;
}

var sequenceDoc = JObject.Parse(sequenceString);

//document is deleted so we are finished.
if (sequenceDoc["deleted"]?.Value<bool>() == true ||
   string.IsNullOrWhiteSpace(sequenceDoc?["doc"]?.ToString()))
{
   return;
}

var docId = sequenceDoc["doc"]["_id"].ToString();

var sequenceNumber = sequenceDoc["seq"].Value<string>().Split('-')[0];

if (string.IsNullOrWhiteSpace(sequenceNumber))
{
   return;
}

if (sequenceDoc["doc"]["doc_type"] == null)
{
   return;
}

//TOFO: define the document we are looking for.
//           if (!sequenceDoc["doc"]["doc_type"].Compare(DocumentTypes.Chart_Change)) //todo: this should be configurable.
//           {
//               //_log.Debug($"doc_type {sequenceDoc["doc"]["doc_type"]} is not valid. Stopping Changehandler.");
//               return;
//           }

if (await SequenceLocked(DocumentTypes.LockingDocType, "", 0))
   return;

if (Handlers != null)
   Handlers.Handle(this, new EventArgs());
*/
        }

        public async Task<bool> SequenceLocked(DocumentTypes docType, string sequenceId, int attempt)
        {
            try
            {
                var lockDoc = new LockDocument(docType, sequenceId, attempt);
                var response = await _couchClient.Documents.PostAsync(lockDoc.ToString());

                return !response.IsSuccess;
            }
            catch (Exception ex)
            {
                return false;
            }
        }  

        public void Dispose()
        {

        }
    }
}
