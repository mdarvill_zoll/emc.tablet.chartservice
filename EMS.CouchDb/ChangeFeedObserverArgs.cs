﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMS.CouchDb.Observers
{
    public class ChangeFeedObserverArgs
    {
        public string ChangesFilter { get; set; }

        //public string LockingDocType { get; set; }
    }
}
